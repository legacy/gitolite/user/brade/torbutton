<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Perihal Tor">

<!ENTITY aboutTor.viewChangelog.label "Lihat Log Perubahan">

<!ENTITY aboutTor.ready.label "Menjelajahlah. Secara Persendirian.">
<!ENTITY aboutTor.ready2.label "Anda kini bersedia menikmati pengalaman melayari Internet secara persendirian.">
<!ENTITY aboutTor.failure.label "Sesuatu telah berlaku!">
<!ENTITY aboutTor.failure2.label "Tor tidak berfungsi ke atas pelayar ini.">

<!ENTITY aboutTor.search.label "Gelintar dengan DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "Ada Pertanyaan?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Periksa Panduan Pelayar Tor kami »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "Panduan Pelayar Tor">

<!ENTITY aboutTor.tor_mission.label "Projek Tor ialah organisasi bukan-berasaskan-keuntungan US 501(c)(3) yang melindungi hak-hak kemanusian dan kebebasan dengan mencipta dan menghasilkan teknologi keawanamaan dan kerahsiaan bebas dan bersumber-terbuka, menyokong ketersediaan dan penggunaan tanpa-had, dan melanjutkan kesefahaman saintifik dan popular mereka.">
<!ENTITY aboutTor.getInvolved.label "Sertai »">

<!ENTITY aboutTor.newsletter.tagline "Dapatkan berita terkini Tor yang terus ke dalam kotak mel anda.">
<!ENTITY aboutTor.newsletter.link_text "Daftar untuk dapatkan Berita Tor.">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor sepenuhnya percuma digunakan kerana adanya derma oleh individu seperti anda.">
<!ENTITY aboutTor.donationBanner.buttonA "Dermalah Sekarang">

<!ENTITY aboutTor.alpha.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.alpha.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.alpha.bannerDescription "Tor Browser Alpha is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.alpha.bannerLink "Report a bug on the Tor Forum">

<!ENTITY aboutTor.nightly.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.nightly.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.nightly.bannerDescription "Tor Browser Nightly is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.nightly.bannerLink "Report a bug on the Tor Forum">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "POWERED BY PRIVACY:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "RESISTANCE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "CHANGE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "FREEDOM">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "BERI DERMA SEKARANG">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "Your donation will be matched by Friends of Tor, up to $100,000.">
