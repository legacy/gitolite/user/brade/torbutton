# For Tor Browser, we use a new file (different than the brand.ftl file
# that is used by Firefox) to avoid picking up the -brand-short-name values
# that Mozilla includes in the Firefox language packs.

-brand-shorter-name = Tor ဘရောင်ဇာ
-brand-short-name = Tor ဘရောင်ဇာ
-brand-full-name = Tor ဘရောင်ဇာ
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = Tor ဘရောင်ဇာ
-vendor-short-name = Tor စီမံကိန်း ပရောဂျက်
trademarkInfo = 'Tor' နှင့် 'Onion Logo' (ကြက်သွန်နီအမှတ်တံဆိပ်သင်္ကေတ) သည် Tor Project, Inc. ၏ မှတ်ပုံတင်ထားသော ကုန်အမှတ်တံဆိပ်ဖြစ်သည်။
