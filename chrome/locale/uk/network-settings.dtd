<!ENTITY torsettings.dialog.title "Налаштування мережі Tor">
<!ENTITY torsettings.wizard.title.default "Зʼєднатися з Tor">
<!ENTITY torsettings.wizard.title.configure "Налаштування мережі Tor">
<!ENTITY torsettings.wizard.title.connecting "Встановлення зʼєднання">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Мова вебоглядача Tor">
<!ENTITY torlauncher.localePicker.prompt "Будь ласка, оберіть мову.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Натисніть кнопку «Зʼєднати», щоб зʼєднатися з Tor.">
<!ENTITY torSettings.configurePrompt "Натисніть «Налаштувати», щоб налаштувати параметри мережі, якщо Ви перебуваєте в країні, яка цензурує Tor (наприклад, Єгипет, Китай, Туреччина) або якщо Ви підʼєднуєтеся з приватної мережі, для якої потрібен проксі-сервер.">
<!ENTITY torSettings.configure "Налаштування">
<!ENTITY torSettings.connect "З'єднатися">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Підключення до Tor...">
<!ENTITY torsettings.restartTor "Перезапустити Tor">
<!ENTITY torsettings.reconfigTor "Переналаштувати">

<!ENTITY torsettings.discardSettings.prompt "Ви налаштували мости Tor або ввели налаштування локального проксі.&#160; Для прямого з'єднання з мережею Tor ці налаштування треба вилучити.">
<!ENTITY torsettings.discardSettings.proceed "Вилучити налаштування і під'єднатися">

<!ENTITY torsettings.optional "Додатково">

<!ENTITY torsettings.useProxy.checkbox "Я використовую проксі для з'єднання з Інтернетом">
<!ENTITY torsettings.useProxy.type "Тип проксі">
<!ENTITY torsettings.useProxy.type.placeholder "оберіть тип проксі">
<!ENTITY torsettings.useProxy.address "Адреса">
<!ENTITY torsettings.useProxy.address.placeholder "ІР адреса або ім'я хосту">
<!ENTITY torsettings.useProxy.port "Порт">
<!ENTITY torsettings.useProxy.username "Ім'я користувача">
<!ENTITY torsettings.useProxy.password "Пароль">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Мій брандмауер дозволяє підключення тільки до певних портів">
<!ENTITY torsettings.firewall.allowedPorts "Дозволені порти">
<!ENTITY torsettings.useBridges.checkbox "Тор цензується в моїй країні">
<!ENTITY torsettings.useBridges.default "Виберіть вбудований міст">
<!ENTITY torsettings.useBridges.default.placeholder "Виберіть міст">
<!ENTITY torsettings.useBridges.bridgeDB "Запитати міст з torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Введіть символи з зображення">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Отримати нове завдання">
<!ENTITY torsettings.useBridges.captchaSubmit "Надіслати">
<!ENTITY torsettings.useBridges.custom "Надайте міст, який я знаю">
<!ENTITY torsettings.useBridges.label "Уведіть інформацію про міст із надійного джерела.">
<!ENTITY torsettings.useBridges.placeholder "тип адреси: порт (по одному на рядок)">

<!ENTITY torsettings.copyLog "Скопіювати журнал Tor до буферу обміну">

<!ENTITY torsettings.proxyHelpTitle "Довідка Проксі">
<!ENTITY torsettings.proxyHelp1 "Може знадобитися локальний проксі-сервер при підключенні через мережу компанії, школи або університету.&#160;Якщо ви не впевнені у необхідності проксі-серверу, погляньте у налаштування з\єднання з інтернетом у іншому браузері або перевірте ваші системні налаштування мережі.">

<!ENTITY torsettings.bridgeHelpTitle "Допомога по ретрансляторам типу міст">
<!ENTITY torsettings.bridgeHelp1 "Мости - це незареєстровані передавачі, що ускладнюють блокування з'єднання з мережею Tor.&#160; Кожен тип мосту використовує інакший метод, щоб уникнути цензури.&#160;  Функції obfs роблять ваш трафік схожим на випадковий шум, а функції meek роблять ваш трафік схожим на те, що він підключається до цієї служби не через Tor.">
<!ENTITY torsettings.bridgeHelp2 "Через те, як певні країни намагаються заблокувати Tor, деякі мости працюють в деяких країнах, а в інших ні.&#160; Якщо ви не впевнені в тому, які мости працюють у вашій країні, відвідайте torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Будь ласка, зачекайте, поки ми встановимо з'єднання з мережею Tor.&#160; Це може зайняти кілька хвилин.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Зʼєднання">
<!ENTITY torPreferences.torSettings "Налаштування Tor">
<!ENTITY torPreferences.torSettingsDescription "Tor Browser спрямовує ваш трафік через мережу Tor, яка підтримується тисячами добровольців по всьому світу." >
<!ENTITY torPreferences.learnMore "Дізнатися більше">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Інтернет:">
<!ENTITY torPreferences.statusInternetTest "Тест">
<!ENTITY torPreferences.statusInternetOnline "У мережі">
<!ENTITY torPreferences.statusInternetOffline "Поза мережею">
<!ENTITY torPreferences.statusTorLabel "Мережа Tor:">
<!ENTITY torPreferences.statusTorConnected "Зʼєднано">
<!ENTITY torPreferences.statusTorNotConnected "Не під'єднано">
<!ENTITY torPreferences.statusTorBlocked "Потенційно заблоковано">
<!ENTITY torPreferences.learnMore "Дізнатися більше">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Quickstart">
<!ENTITY torPreferences.quickstartDescriptionLong "Швидкий запуск автоматично підключає браузер Tor до мережі Tor під час запуску на основі останніх використаних налаштувань підключення.">
<!ENTITY torPreferences.quickstartCheckbox "Завжди з'єднуватись автоматично">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Мости">
<!ENTITY torPreferences.bridgesDescription "Мости допомагають отримати доступ до мережі Tor у місцях, де Tor заблоковано. Залежно від того, де ви знаходитесь, один міст може працювати краще, ніж інший.">
<!ENTITY torPreferences.bridgeLocation "Ваше розташування">
<!ENTITY torPreferences.bridgeLocationAutomatic "Автоматично">
<!ENTITY torPreferences.bridgeLocationFrequent "Локації які вибираються найчастіше">
<!ENTITY torPreferences.bridgeLocationOther "Інші локації">
<!ENTITY torPreferences.bridgeChooseForMe "Обрати для мене міст…">
<!ENTITY torPreferences.bridgeBadgeCurrent "Ваші поточні мости">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "You can save one or more bridges, and Tor will choose which one to use when you connect. Tor will automatically switch to use another bridge when needed.">
<!ENTITY torPreferences.bridgeId "#1 міст: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "Вилучити">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Вимкнути вбудовані мости">
<!ENTITY torPreferences.bridgeShare "Поділіться цим мостом за допомогою QR-коду або скопіювавши його адресу:">
<!ENTITY torPreferences.bridgeCopy "Копіювати адресу мосту">
<!ENTITY torPreferences.copied "Скопійовано!">
<!ENTITY torPreferences.bridgeShowAll "Показати всі мости">
<!ENTITY torPreferences.bridgeRemoveAll "Видалити всі мости">
<!ENTITY torPreferences.bridgeAdd "Додати новий міст">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Обрати один із вбудованих мостів браузера Tor">
<!ENTITY torPreferences.bridgeSelectBuiltin "Обрати вбудований міст...">
<!ENTITY torPreferences.bridgeRequest "Запросити міст...">
<!ENTITY torPreferences.bridgeEnterKnown "Введіть адресу мосту, який вам вже відомий">
<!ENTITY torPreferences.bridgeAddManually "Додати міст вручну…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "Розширений">
<!ENTITY torPreferences.advancedDescription "Налаштуйте спосіб підключення браузера Tor до Інтернету">
<!ENTITY torPreferences.advancedButton "Налаштування...">
<!ENTITY torPreferences.viewTorLogs "Переглянути журнали Tor">
<!ENTITY torPreferences.viewLogs "Переглянути журнали...">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Прибрати всі мости?">
<!ENTITY torPreferences.removeBridgesWarning "Цю дію не можна буде відмінити">
<!ENTITY torPreferences.cancel "Скасувати">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Сканувати QR код">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Вбудовані мости">
<!ENTITY torPreferences.builtinBridgeDescription "Браузер Tor включає деякі специфічні типи мостів, відомі як «транспорти, що підключаються».">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 є типом вбудованого моста, завдяки якому ваш трафік Tor виглядає випадковим. Вони рідше будуть заблоковані, ніж їхні попередники, мости obfs3.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake є вбудованим мостом, який долає цензуру, перенаправляючи ваше з’єднання через проксі-сервери Snowflake, якими керують волонтери.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure є вбудованим мостом, який створює враження, ніби ви використовуєте веб-сайт Microsoft замість Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Запросити міст">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Звернення до BridgeDB. Зачекайте, будь ласка.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Розв'яжіть CAPTCHA, щоб надіслати запит на отримання мосту.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "Розв'язання невірне. Спробуйте знову.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Надати міст">
<!ENTITY torPreferences.provideBridgeHeader "Введіть інформацію про міст із надійного джерела">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Налаштування з'єднання">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Налаштуйте спосіб підключення браузера Tor до Інтернету">
<!ENTITY torPreferences.firewallPortsPlaceholder "Значення, розділені комами">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Журнали Tor">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Не під'єднано">
<!ENTITY torConnect.connectingConcise "Під'єднання...">
<!ENTITY torConnect.tryingAgain "Спробувати ще раз...">
<!ENTITY torConnect.noInternet "Браузер Tor не зміг під'єднатись до інтернету">
<!ENTITY torConnect.noInternetDescription "This could be due to a connection issue rather than Tor being blocked. Check your Internet connection, proxy and firewall settings before trying again.">
<!ENTITY torConnect.couldNotConnect "Браузер Tor не може підключитися до Tor">
<!ENTITY torConnect.assistDescriptionConfigure "налаштуйте ваше з'єднання"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "Якщо Tor заблоковано у вашому місцезнаходженні, випробовування мосту може допомогти. Помічник з'єднання допоможе обрати мост для вас, використовуючи ваше місцезнаходження, або ви можете натомість #1 вручну."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Випробовуємо мост…">
<!ENTITY torConnect.tryingBridgeAgain "Trying one more time…">
<!ENTITY torConnect.errorLocation "Браузер Tor не зміг вас знайти">
<!ENTITY torConnect.errorLocationDescription "Браузер Tor повинен знати ваше місцезнаходження, щоб обрати потрібний для вас міст. Якщо ви не хочете ділитися своїм місцезнаходженням, натомість #1 вручну."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Are these location settings correct?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser still couldn’t connect to Tor. Please check your location settings are correct and try again, or #1 instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.finalError "Tor Browser still cannot connect">
<!ENTITY torConnect.finalErrorDescription "Despite its best efforts, connection assist was not able to connect to Tor. Try troubleshooting your connection and adding a bridge manually instead.">
<!ENTITY torConnect.breadcrumbAssist "Допомога з'єднання">
<!ENTITY torConnect.breadcrumbLocation "Налаштування місцезнаходження">
<!ENTITY torConnect.breadcrumbTryBridge "Випробувати мост">
<!ENTITY torConnect.automatic "Автоматично">
<!ENTITY torConnect.selectCountryRegion "Обрати країну чи регіон">
<!ENTITY torConnect.frequentLocations "Локації які вибираються найчастіше">
<!ENTITY torConnect.otherLocations "Інші локації">
<!ENTITY torConnect.restartTorBrowser "Перезапустити Tor Browser">
<!ENTITY torConnect.configureConnection "Налаштування з'єднання...">
<!ENTITY torConnect.viewLog "Переглянути журнали...">
<!ENTITY torConnect.tryAgain "Спробувати ще раз">
<!ENTITY torConnect.offline "Internet not reachable">
<!ENTITY torConnect.connectMessage "Зміни до налаштувань Tor не застосуються, доки ви з'єднані ">
<!ENTITY torConnect.tryAgainMessage "Вебоглядачу Tor не вдалось з'єднатись з мережею Tor">
<!ENTITY torConnect.yourLocation "Ваше розташування">
<!ENTITY torConnect.tryBridge "Випробувати мост">
<!ENTITY torConnect.autoBootstrappingFailed "Невдале автоматичне налаштування">
<!ENTITY torConnect.autoBootstrappingFailed "Невдале автоматичне налаштування">
<!ENTITY torConnect.cannotDetermineCountry "Не вдається визначити країну користувача">
<!ENTITY torConnect.noSettingsForCountry "Немає доступних налаштувань для вашого місцезнаходження">
